import {BrowserRouter,Switch,Route,Redirect} from 'react-router-dom'
import {Home} from './Pages/Home'
import {Contact} from './Pages/Contact'

export const App = ()=> {
  return (
   <BrowserRouter>
    <Switch>
      <Route exact path='/' component={Home} />
      <Route exact path='/Contact' component={Contact} />
      <Redirect to='/' />
    </Switch>
   </BrowserRouter>
  )
}



import styled from "styled-components"

const TextComponet=styled.h2`
    color: ${props => props.color};
    font-size: ${props => props.fontSize};
    &:hover{
        background-color: ${props => props.backgroundColor} ;
    }
`

export const Text = (props) => {
    return (
        <TextComponet 
            color={props.color}
            fontSize={props.fontSize}
            backgroundColor={props.backgroundColor}
            >
                {props.text}
        </TextComponet>
    )
};


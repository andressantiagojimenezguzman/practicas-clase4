import { Container } from "../Component/Home/Container"
import { Text } from "../Component/Home/Text"

export const Home = () =>{
    return(
        <Container>
            <Text
                text='Home'
                color='red'
                fontSize= '3em'
                backgroundColor='#FFFFFF'/>   
            <Text
                text='Hola'
                color='purple'
                fontSize= '5em'/>   
        </Container>

    )
}
